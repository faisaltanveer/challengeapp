//
//  SignUpViewController.h
//  RoriApp
//
//  Created by Hassan on 12/9/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
#pragma mark - Views outlets
@property (weak, nonatomic) IBOutlet UIView *viewProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfile;
#pragma mrak - textField outlets
@property (weak, nonatomic) IBOutlet UITextField *textFieldUserName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAge;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldContact;

#pragma mark - Buttons action methods
- (IBAction)btn_SignUp:(id)sender;
- (IBAction)btn_back:(id)sender;



@end
