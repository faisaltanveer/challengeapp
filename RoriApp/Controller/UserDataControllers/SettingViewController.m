//
//  SettingViewController.m
//  RoriApp
//
//  Created by Hassan on 12/18/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController
@synthesize textFieldUsername,textFieldContact;
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
   [self setup_Controls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Custom methods
-(void)setup_Controls
{
    textFieldUsername.text=AppUtil.cUser.name;
    textFieldContact.text=AppUtil.cUser.phoneNumber;
}
-(void)updateInfo
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:textFieldUsername.text forKey:@"user_name"];
    [dict setObject:textFieldContact.text forKey:@"user_phone"];
    [dict setValue:[NSNumber numberWithInteger:AppUtil.cUser.userId] forKey:@"user_id"];
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userUpdatedInfo:) name:Notification_UpdateInfo object:nil];
    [AppServices updateInfo:dict];
}
-(void)userUpdatedInfo:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_UpdateInfo object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    NSString *msg=[note.userInfo objectForKey:@"msg"];
    if ([status isEqualToString:@"success"]) {
        [AppUtil displayAlertWithTitle:AlertTitle AndMessage:msg];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}
#pragma mark - Buttons action methods
- (IBAction)btn_logout:(id)sender {
    [UserDefaults removeObjectForKey:kCurrentUser];
    [UserDefaults synchronize];
    AppUtil.cUser=nil;
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_update:(id)sender {
    if (isEmpty(textFieldUsername.text) || isEmpty(textFieldContact.text))
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:@"Please fill all fields"];
    }
    else
    {
        [self updateInfo];
    }
}
@end
