//
//  ChallengeCategory.h
//  RoriApp
//
//  Created by Hassan on 22/12/2014.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChallengeCategory : NSObject
@property (nonatomic,strong)NSString *nameCategory,*categoryID;
@property(nonatomic,strong)NSDate *dateCreation;
@end
