//
//  ProfileViewClass.h
//  RoriApp
//
//  Created by Hassan on 12/18/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewClass : UIViewController
#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblChallengeGiven;
@property (weak, nonatomic) IBOutlet UILabel *lblChallengeCompleted;
@property (weak, nonatomic) IBOutlet UILabel *lblUserRating;
#pragma mark - Buttons action methods
- (IBAction)btn_back:(id)sender;

@end
