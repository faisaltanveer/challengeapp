//
//  ChallengesViewController.m
//  RoriApp
//
//  Created by Hassan on 22/12/2014.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "ChallengesViewController.h"

@interface ChallengesViewController ()
{
    CGFloat scrollHeight;
}
@end

@implementation ChallengesViewController
@synthesize arrChallenges,sViewChallenges;
- (void)viewDidLoad {
#pragma mark - Views loading life cycle methods
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup_Controls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Custom methods
-(void)setup_Controls
{
    [self setup_challenges];
}
-(void)setup_challenges
{
    CGFloat y=0;
    for (zChallenge *chObj in arrChallenges) {
        [self generateChallengeWithName:chObj.nameChallenge andChID:chObj.IDChellenge.integerValue andPosY:y];
        y+=80;
    }
    [sViewChallenges setContentSize:CGSizeMake(320, scrollHeight)];
}
-(void)generateChallengeWithName:(NSString*)name andChID:(NSInteger)chelngID andPosY:(CGFloat)posY
{
    UIButton *btnChellenge = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnChellenge setTitle:name forState:UIControlStateNormal];
    [btnChellenge setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnChellenge.titleLabel setTextAlignment: NSTextAlignmentCenter];
    btnChellenge.frame=CGRectMake(0, posY, 320, 60);
    [btnChellenge setTag:chelngID];
    [btnChellenge addTarget:self action:@selector(btn_ChallengAction:) forControlEvents:UIControlEventTouchUpInside];
    [sViewChallenges addSubview:btnChellenge];
    scrollHeight=btnChellenge.frame.origin.y+70;
}
#pragma mark - Buttons action methods
-(void)btn_ChallengAction:(UIButton*)btnChellenge
{
    NSLog(@"btn category tag==%d",btnChellenge.tag);
    zChallenge *tappedChallenge=[arrChallenges objectAtIndex:btnChellenge.tag];
    
}
- (IBAction)btn_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
