//
//  DrawingViewController.h
//  RoriApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmoothedBIView.h"
@interface DrawingViewController : UIViewController
#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet SmoothedBIView *DrawingView;
@property (weak, nonatomic) IBOutlet UISlider *PencilSizeSlider;
@property (weak, nonatomic) IBOutlet UIButton *PencilSizeButton;
#pragma mark - Buttons action methods
- (IBAction)PencilSizeSlider_Value_Changed:(id)sender;
- (IBAction)Done_Tapped:(id)sender;
- (IBAction)btnEraser:(id)sender;
- (IBAction)Colour_Change_Button_Tapped:(id)sender;
@end
