//
//  SettingViewController.h
//  RoriApp
//
//  Created by Hassan on 12/18/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldContact;



#pragma mark - Buttons action methods
- (IBAction)btn_logout:(id)sender;
- (IBAction)btn_back:(id)sender;
- (IBAction)btn_update:(id)sender;

@end
