//
//  DrawingViewController.m
//  RoriApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "DrawingViewController.h"
#import "ChallengeDetailView.h"
@interface DrawingViewController ()
{
    UIImage *imgDrawn;
}
@end
@implementation DrawingViewController
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpControls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action method
- (IBAction)Colour_Change_Button_Tapped:(id)sender
{
    UIButton *TappedButton = (UIButton *)sender;
    
    UIColor *DrawingBackgroundColour = TappedButton.backgroundColor;
    self.DrawingView.Color = DrawingBackgroundColour;
    [self Update_Slider];
}
- (IBAction)Done_Tapped:(id)sender
{
    if (self.DrawingView.FinalImage != nil)
    {
        imgDrawn = self.DrawingView.FinalImage;
    }
    ChallengeDetailView *cVC=[self.storyboard instantiateViewControllerWithIdentifier:@"challengeDetail"];
    cVC.imgChallenge=imgDrawn;
    [self.navigationController pushViewController:cVC animated:YES];
}
- (IBAction)btnEraser:(id)sender {
    
    self.DrawingView.Color = [UIColor clearColor];
    [self Update_Slider];
}
- (IBAction)PencilSizeSlider_Value_Changed:(id)sender
{
    self.DrawingView.Path.lineWidth = self.PencilSizeSlider.value*10;
    [self Update_Slider];
}
#pragma mark - Custom methods
-(void)setUpControls
{
    self.DrawingView.Color = [UIColor redColor];
    self.PencilSizeSlider.value = self.DrawingView.Path.lineWidth / 10.0;
    [self Update_Slider];
}
-(void)Update_Slider
{
    self.PencilSizeButton.backgroundColor = self.DrawingView.Color;
    self.PencilSizeButton.layer.cornerRadius = 90.0 - 3.0 * self.DrawingView.Path.lineWidth;
}
@end
