//
//  DBManager.m
//  RoriApp
//
//  Created by Hassan on 12/11/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "DBManager.h"

@implementation DBManager

+(DBManager *)sharedWHNUtility
{
    static DBManager *sharedInstance=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance =[[DBManager alloc]init];
        sharedInstance=[[self alloc]init];
    }
    );
    return sharedInstance;
}
#pragma mark - Core data methods
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(void)saveContext
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    // Save the object to persistent store
    
    if (![context save:&error])
    {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}
/*
- (void)saveWallpapers:(NSArray *)wallpapersArray
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    Wallpapers *newWallPaper;
    if (wallpapers.count<1)
    {
        for (NSString *wall in wallpapersArray)
        {
            newWallPaper = [NSEntityDescription insertNewObjectForEntityForName:@"Wallpapers" inManagedObjectContext:context];
            newWallPaper.wallpaper=wall;
            newWallPaper.isFav=[NSNumber numberWithBool:NO];
        }
    }
    [self saveContext];
}


-(void)setFavWall:(Wallpapers *)favWall
{
    BOOL isFav = [favWall.isFav boolValue];
    if (isFav)
    {
        favWall.isFav=[NSNumber numberWithBool:NO];
    }
    else
        favWall.isFav=[NSNumber numberWithBool:YES];
    
    NSLog(@"isFavort %@",favWall.isFav);
    [self saveContext];
    [self fetchWallpapers];
}
- (void)fetchWallpapers
{
    // Fetch the wallpapers from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Wallpapers"];
    wallpapers = [managedObjectContext executeFetchRequest:fetchRequest error:nil] ;
    // NSLog(@"wallpapers: %d",wallpapers.count);
}
- (void)fetchFavWallpapers
{
    // Fetch the favourite wallpapers from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Wallpapers"];
    NSPredicate *favWallpaper= [NSPredicate predicateWithFormat:@"isFav == YES"];
    [fetchRequest setPredicate:favWallpaper];
    wallpapers = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog(@"favwallpapers: %d",wallpapers.count);
    isFavController=YES;
}
*/
@end
