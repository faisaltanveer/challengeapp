//
//  zUser.m
//  SecondHandBooks
//
//  Created by Azeem Akram on 25/02/2013.
//  Copyright (c) 2013 @zeem. All rights reserved.
//

#import "zUser.h"

@implementation zUser

@synthesize name , password , email , groupId,completeChallenges,totalChallenges;
@synthesize fullName , imagePath ,isNotificationActive , userId,typeId,isBlocked,userRating;
@synthesize phoneNumber;

-(id )initWithID:(NSInteger)usrID andTypeId:(NSInteger)typId  andEmail:(NSString *)zEmail  andPassword:(NSString *)pwd andUserName:(NSString *)nme andGroupId:(NSInteger)zgrpId andFullNameIs:(NSString *)fname andUserImageis:(NSString *)imagepath andIsNotificationActv:(BOOL)ntAcv andIsBlock:(BOOL)isBlock
{
    self = [super init];
    if (self)
    {
        self.userId                 = usrID;
        self.typeId                 = typId;
        self.groupId                = zgrpId;
        self.name                   = nme;
        self.email                  = zEmail ;
        self.password               = pwd;
        self.imagePath              = imagepath;
        self.fullName               = fname;
        self.isNotificationActive   = ntAcv;
        self.isBlocked              = isBlock;
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeInteger:self.userId forKey:@"userId"];
    [coder encodeInteger:self.groupId forKey:@"groupId"];
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeObject:self.completeChallenges forKey:@"completeChallenges"];
    [coder encodeObject:self.totalChallenges forKey:@"totalChallenges"];
    [coder encodeObject:self.email forKey:@"email"];
    [coder encodeObject:self.password forKey:@"pwd"];
    [coder encodeObject:self.imagePath forKey:@"imagePath"];
    [coder encodeObject:self.fullName forKey:@"fullName"];
    [coder encodeBool:self.isNotificationActive forKey:@"isNotificationActive"];
}
-(id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.userId                 = [decoder decodeIntForKey:@"userId"];
        self.groupId                = [decoder decodeIntForKey:@"groupId"];
        self.name                   = [decoder decodeObjectForKey:@"name"];
        self.completeChallenges     = [decoder decodeObjectForKey:@"completeChallenges"];
        self.totalChallenges        = [decoder decodeObjectForKey:@"totalChallenges"];
        self.email                  = [decoder decodeObjectForKey:@"email"];
        self.password               = [decoder decodeObjectForKey:@"password"];
        self.imagePath              = [decoder decodeObjectForKey:@"imagePath"];
        self.isNotificationActive   = [decoder decodeBoolForKey:@"isNotificationActive"];
    }
    return self;
}
@end
