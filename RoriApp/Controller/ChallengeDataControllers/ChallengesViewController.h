//
//  ChallengesViewController.h
//  RoriApp
//
//  Created by Hassan on 22/12/2014.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChallengesViewController : UIViewController
@property(nonatomic,strong)NSMutableArray *arrChallenges;
#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIScrollView *sViewChallenges;

#pragma mark - Buttons acton methods
- (IBAction)btn_back:(id)sender;

@end
