//
//  ChallengeCategoriesVIew.m
//  RoriApp
//
//  Created by Hassan on 22/12/2014.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "ChallengeCategoriesVIew.h"
#import "ChallengesViewController.h"
@interface ChallengeCategoriesVIew ()
{
    NSMutableArray *arrChlngeCategories;
    NSMutableArray *arrChlnge;
}
@end

@implementation ChallengeCategoriesVIew
@synthesize sViewCategories;
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup_Controls];
    [self getChallengeCategories];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Custom methods
-(void)setup_Controls
{
    arrChlngeCategories=[[NSMutableArray alloc]init];
    arrChlnge          =[[NSMutableArray alloc]init];
}
-(void)getChallengeCategories
{
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userGotCategories:) name:Notification_GetChallengeCategories object:nil];
    [AppServices getChallengeCategories];
}
-(void)extractCategoriesFromArray:(NSArray*)arrCategories
{
    CGFloat y=0;
    for (NSDictionary *cObj in arrCategories) {
        ChallengeCategory *catObj=[[ChallengeCategory alloc]init];
        catObj.nameCategory=[cObj objectForKey:@"cat_name"];
        catObj.categoryID=[cObj objectForKey:@"cat_id"];
        NSLog(@"%@",[cObj objectForKey:@"cat_create_date"]);
        catObj.dateCreation=[cObj objectForKey:@"cat_create_date"];
        [arrChlngeCategories addObject:catObj];
        [self generateCategoryWithName:catObj.nameCategory andCatID:catObj.categoryID.integerValue andPosY:y];
        y+=80;
    }
}
-(void)extractChallengesFromArray:(NSArray*)arrCategories
{
    if (arrChlnge.count>0) {
        [arrChlnge removeAllObjects];
    }
    for (NSDictionary *cObj in arrCategories) {
        zChallenge *chObj=[[zChallenge alloc]init];
        chObj.nameChallenge=[cObj objectForKey:@"game_name"];
        chObj.IDChellenge=[cObj objectForKey:@"game_id"];
        chObj.IDCatChallenge=[cObj objectForKey:@"game_cate"];
        chObj.userIDChallenge=[cObj objectForKey:@"game_user_id"];
        chObj.status=[cObj objectForKey:@"game_status"];
        NSLog(@"data==%@ and name==%@",[cObj objectForKey:@"game_create_date"],chObj.nameChallenge);
        chObj.dateCreation=[cObj objectForKey:@"game_create_date"];
        [arrChlnge addObject:chObj];
    }
    ChallengesViewController *cVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ChallengesScreen"];
    cVC.arrChallenges=arrChlnge;
    NSLog(@"arr count==%d and before==%d",cVC.arrChallenges.count,arrChlnge.count);
    [self.navigationController pushViewController:cVC animated:YES];
}
-(void)generateCategoryWithName:(NSString*)name andCatID:(NSInteger)catID andPosY:(CGFloat)posY
{
    UIButton *btnCategory = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnCategory setTitle:name forState:UIControlStateNormal];
    [btnCategory setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnCategory.titleLabel setTextAlignment: NSTextAlignmentCenter];
    btnCategory.frame=CGRectMake(0, posY, 320, 60);
    [btnCategory setTag:catID];
    [btnCategory addTarget:self action:@selector(btn_CatAction:) forControlEvents:UIControlEventTouchUpInside];
    [sViewCategories addSubview:btnCategory];
}
#pragma mark - Notify methods
-(void)userGotCategories:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_GetChallengeCategories object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    NSString *msg=[note.userInfo objectForKey:@"msg"];
    if ([status isEqualToString:@"success"])
    {
        NSArray *arrCategory=[note.userInfo objectForKey:@"game_list"];
        NSLog(@"categories array==%@",arrCategory);
        [self extractCategoriesFromArray:arrCategory];
    }
    else
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}
-(void)userGotChallenges:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_GetChallenges object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    NSString *msg=[note.userInfo objectForKey:@"message"];
    if ([status isEqualToString:@"success"])
    {
        NSArray *arrChalenges=[note.userInfo objectForKey:@"game_list"];
        NSLog(@"challenges array==%@",arrChalenges);
        [self extractChallengesFromArray:arrChalenges];
    }
    else
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}

#pragma mark - Buttons action methods
-(void)btn_CatAction:(UIButton*)btnCategory
{
    NSLog(@"btn category tag==%d",btnCategory.tag);
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[NSNumber numberWithInteger:btnCategory.tag] forKey:@"cat_id"];
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userGotChallenges:) name:Notification_GetChallenges object:nil];
    [AppServices getChallengesForCategory:dict];
}
- (IBAction)btn_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
