//
//  AppUtility.m
//  Sensor Logging
//
//  Created by Lion User on 02/09/2013.
//  Copyright (c) 2013 Adroit. All rights reserved.
//

#import "AppUtility.h"
#import "AppDelegate.h"

@implementation AppUtility

+(AppUtility *)sharedWHNUtility
{
    static AppUtility *sharedInstance=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance =[[AppUtility alloc]init];
        sharedInstance=[[self alloc]init];
    }
);
    return sharedInstance;
}
#pragma mark - Custom methods

-(void)checkNewVersion
{
    float appVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
    NSLog(@"app version %f",appVersion);
    NSString * versionUrl = @"https://bstnweb.bstn.org/iApps/Version/smores.html";
    NSData * webData = [NSData dataWithContentsOfURL:[NSURL URLWithString:versionUrl]];
    if(webData == nil)
    {
        NSLog(@"No data Found");
    }
    else
    {
        NSString * responseString = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
        responseString = [responseString substringFromIndex:2];
        responseString = [responseString substringToIndex:[responseString length]-2];
        NSError * error;
        NSArray *json =
        [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers
            error: &error];
        float version = [[json valueForKey:@"Version"] floatValue];
        NSLog(@"version is %f",version);
        if(appVersion < version)
        {
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            NSDate *now = [NSDate date];
            localNotification.fireDate = now;
            localNotification.alertBody =
            [NSString stringWithFormat:@"New Version of \"Smores App\" %.2f is available",version];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
    }
}
+ (CLLocation*) geoCodeUsingAddress: (NSString *) address
{
    CLLocation *myLocation;
    
    // -- modified from the stackoverflow page - we use the SBJson parser instead of the string scanner --
    NSString *esc_addr = [address stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat: @"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSDictionary *googleResponse = [[NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL] objectFromJSONString];
    NSLog(@"json response == %@",googleResponse);
    NSDictionary    *resultsDict = [googleResponse valueForKey:@"results"];   // get the results dictionary
    NSDictionary   *geometryDict = [resultsDict valueForKey: @"geometry"];   // geometry dictionary within the  results dictionary
    NSDictionary   *locationDict = [  geometryDict valueForKey: @"location"];   // location dictionary within the geometry dictionary
    
    // -- you should be able to strip the latitude & longitude from google's location information (while understanding what the json parser returns) --
    
    NSLog(@"-- returning latitude & longitude from google --");
    
    NSArray *latArray = [locationDict valueForKey: @"lat"];
    NSString *latString = [latArray lastObject];
    NSArray *lngArray = [locationDict valueForKey: @"lng"];
    NSString *lngString = [lngArray lastObject];
    
    myLocation=[[CLLocation alloc]initWithLatitude:[latString doubleValue] longitude:[lngString doubleValue]];
    
    return myLocation;
}
#pragma mark - App utility methods
-(void)setCurrentUserWithInfo:(NSDictionary*)userInfo
{
    zUser *currentUser=[[zUser alloc]init];
    currentUser.name=[userInfo objectForKey:@"user_name"];
    currentUser.userId=[[userInfo objectForKey:@"user_id"]integerValue];
    currentUser.email=[userInfo objectForKey:@"user_email"];
    currentUser.phoneNumber=[userInfo objectForKey:@"user_phone"];
    currentUser.completeChallenges=[userInfo objectForKey:@"completed_chlngs"];
    currentUser.totalChallenges=[userInfo objectForKey:@"receive_chlngs"];
    currentUser.userRating=[NSString stringWithFormat:@"%@",[userInfo objectForKey:@"user_rating"]];
    if ([[userInfo objectForKey:@"user_type"]isEqualToString:@"contact"]) {
        NSString *imgPath=[NSString stringWithFormat:@"%@%@",BaseUrl,[userInfo objectForKey:@"user_pic_url"]];
        currentUser.imagePath=imgPath;
    }
    else
    {
        currentUser.imagePath=[userInfo objectForKey:@"user_pic_url"];
    }
    [self setCustomObject:currentUser forKey:kCurrentUser];
    AppUtil.cUser=currentUser;
}
#pragma mark - General Utility methods
-(void)displayAlertWithTitle:(NSString *)title AndMessage:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
-(BOOL)InternetReachable
{
    /*if ([[Reachability reachabilityForInternetConnection]isReachable]&&[[Reachability reachabilityWithHostName:@"www.google.com"]isReachable]) {
        return YES;
    }
    else
    {
        [self displayAlertWithTitle:AlertTitle AndMessage:@"Internet not available,check your internet connection"];
    }
    return NO;*/
    Reachability *r = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        [self displayAlertWithTitle:AlertTitle AndMessage:@"Internet not available,check your internet connection"];
        return NO;
    }
    else
        return YES;
}
-(UIImage*)scaleToSize:(CGSize)size andimage:(UIImage *)image
{
    UIGraphicsBeginImageContext(size);
    // Draw the scaled image in the current context
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // Create a new image from current context
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Pop the current context from the stack
    UIGraphicsEndImageContext();
    // Return our new scaled image
    return scaledImage;
}
#pragma mark - setting and getting objects methods
- (void)setCustomObject:(id)obj forKey:(NSString *)key
{
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:obj];
    [UserDefaults removeObjectForKey:key];
    [UserDefaults synchronize];
    [UserDefaults setObject:myEncodedObject forKey:key];
}

- (id)getCustomObjectWithKey:(NSString *)key
{
    NSData *myEncodedObject = [UserDefaults objectForKey:key];
    id obj = [NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    return obj;
}

@end
