//
//  AppUtility.h
//  Sensor Logging
//
//  Created by Lion User on 02/09/2013.
//  Copyright (c) 2013 Adroit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import "zUser.h"
#define AppUtil [AppUtility sharedWHNUtility]
#define AppName @"Challenge App"
#define UserDefaults [NSUserDefaults standardUserDefaults]
#define NotificationCenter [NSNotificationCenter defaultCenter]
#define AlertTitle @"Challenge App"

#define MainWindow [[[UIApplication sharedApplication] delegate] window]

@interface AppUtility : NSObject {
    BOOL isCalled;
}
@property(nonatomic,strong) zUser *cUser;
#pragma mark - methods declaration
+(AppUtility *)sharedWHNUtility;
+ (CLLocation*) geoCodeUsingAddress: (NSString *) address;
-(void)displayAlertWithTitle:(NSString *)title AndMessage:(NSString *)msg;
- (void)setCustomObject:(id)obj forKey:(NSString *)key;
- (id)getCustomObjectWithKey:(NSString *)key;
-(UIImage*)scaleToSize:(CGSize)size andimage:(UIImage *)image;
-(BOOL)InternetReachable;
-(void)checkNewVersion;
-(void)setCurrentUserWithInfo:(NSDictionary*)userInfo;
@end

#pragma mark - Object checking Methods

static inline BOOL isEmpty(id thing) {
    return thing == nil
    || [thing isMemberOfClass:[NSNull class]]
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}
static inline BOOL isEmail(id email)
{
    NSString *strEmailMatchstring=@"\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strEmailMatchstring];
    if(!isEmpty(email) && [regExPredicate evaluateWithObject:email])
        return YES;
    else
        return NO;
}