//
//  FbFrnds.h
//  Truly Heal
//
//  Created by Faisal Tanveer on 11/6/13.
//  Copyright (c) 2013 faisaltanveer@relax-solutions.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FbFrnds : NSObject
{
    NSString *frndId;
    NSString *name;
    NSString *picURL;
    BOOL selected;
}
@property (nonatomic,retain)   NSString *name,*frndId,*picURL;
@property (nonatomic , assign) BOOL selected;
@end
