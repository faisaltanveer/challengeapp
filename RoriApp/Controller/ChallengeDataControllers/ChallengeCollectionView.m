//
//  ChallengeCollectionView.m
//  GameApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "ChallengeCollectionView.h"
#import "ChallengeDetailView.h"
#import "DrawingViewController.h"
@interface ChallengeCollectionView ()

@end

@implementation ChallengeCollectionView
@synthesize cViewChallenges;
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup_Controls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action methods

#pragma mark - collection view data source mehtods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UIImageView *itemChallenge=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"image%d.jpg",indexPath.row+1]]];
    cell.backgroundView=itemChallenge;
    cell.backgroundColor = [UIColor purpleColor];
    return cell;
}
#pragma mark - collection view delagte methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"image %ld clicked",(long)indexPath.row);
    DrawingViewController *dVC=[self.storyboard instantiateViewControllerWithIdentifier:@"drawingView"];
    //dVC.imgChallenge=[UIImage imageNamed:[NSString stringWithFormat:@"image%ld.jpg",indexPath.row+1]];
    [self.navigationController pushViewController:dVC animated:YES];
}
#pragma mark - Custom methods
-(void)setup_Controls
{
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)cViewChallenges.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
}
@end
