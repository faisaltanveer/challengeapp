//
//  Services.h
//  DriverModule
//
//  Created by appstudioz on 05/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#define AppServices [Services sharedInstance]
#import <Foundation/Foundation.h>

@interface Services : NSObject<NSXMLParserDelegate>
{
    
}
+ (Services*) sharedInstance;
#pragma mark - User's info methods declaration
-(void)Login:(NSMutableDictionary *)dictionary;
-(void)registerUser:(NSMutableDictionary *)dictionary;
-(void)getUserInfo:(NSMutableDictionary *)dictionary;
-(void)updateInfo:(NSMutableDictionary *)dictionary;
-(void)forgetPassword:(NSMutableDictionary *)dictionary;
#pragma mark - Challenge's info methods
-(void)getChallengeCategories;
-(void)getChallengesForCategory:(NSMutableDictionary*)dictionary;
@end
