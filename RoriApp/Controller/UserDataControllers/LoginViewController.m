//
//  HomeViewController.m
//  GameApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "LoginViewController.h"
#import "FbFrnds.h"
#import "HomeViewController.h"
#import "SignUpViewController.h"
@interface LoginViewController ()
{
    NSString *objID;
    BOOL isFBCall;
}
@end

@implementation LoginViewController
@synthesize textFieldEmail,textFieldPassword;
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup_Controls];
}
-(void)viewDidAppear:(BOOL)animated
{
    if (AppUtil.cUser.name.length>0) {
        if (![self.presentedViewController isBeingDismissed])
        {
            [self dismissViewControllerAnimated:NO completion:nil];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action methods
- (IBAction)btn_login:(id)sender {
    if (isEmpty(self.textFieldPassword.text) || isEmpty(self.textFieldEmail.text))
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:@"Please fill all fields"];
    }
    else if (!isEmail(self.textFieldEmail.text)) {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:@"Incorrect email address"];
    }
    else
    {
        [self signIn];
    }
}

- (IBAction)btn_signUp:(id)sender {
    SignUpViewController *sVC=[self.storyboard instantiateViewControllerWithIdentifier:@"signUp"];
    [self presentViewController:sVC animated:YES completion:nil];
}

- (IBAction)btn_ForgotPassword:(id)sender {
    if (!isEmail(textFieldEmail.text)) {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:@"Incorrect email address"];
    }
    else
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:textFieldEmail.text forKey:@"user_email"];
        [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userForgetPassword:) name:Notification_ForgetPassword object:nil];
        [AppServices forgetPassword:dict];
                
    }
}
#pragma mark - FBLogin deleagte methods

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
user:(id<FBGraphUser>)user
{
    if (isFBCall==NO) {
        [self facebookSignIn:user];
    }
}
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    //self.lblStatus.text = @"You're logged in as";
}
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    self.viewProfile.profileID = nil;
    self.lblName.text = @"";
    //self.lblStatus.text= @"You're not logged in!";
}
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures that happen outside of the app
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}
#pragma mark - Custom methods
-(void)facebookSignIn : (id<FBGraphUser>)user
{
    isFBCall=YES;
    self.viewProfile.profileID = user.objectID;
    objID=user.objectID;
    self.lblName.text = user.name;
    //[self requestFriendsList];
    NSString *email = [user objectForKey:@"email"];
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", user.objectID];
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    NSMutableDictionary *dictparameter=[[NSMutableDictionary alloc]init];
    [dictparameter setObject:user.name forKey:@"user_name"];
    [dictparameter setObject:email forKey:@"user_email"];
    [dictparameter setObject:@"" forKey:@"user_phone"];
    [dictparameter setObject:@"password" forKey:@"user_password"];
    [dictparameter setObject:@"fb" forKey:@"type"];
    [dictparameter setObject:userImageURL forKey:@"user_pic_url"];
    [dictparameter setObject:user.objectID forKey:@"user_fb_id"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSignIn:) name:Notification_Register_User object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSignIn:) name:Notification_Login object:nil];
    [AppServices registerUser:dictparameter];
}
-(void)signIn
{
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    NSMutableDictionary *dictparameter=[[NSMutableDictionary alloc]init];
    [dictparameter setObject:textFieldEmail.text forKey:@"login_id"];
    [dictparameter setObject:textFieldPassword.text forKey:@"user_password"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSignIn:) name:Notification_Login object:nil];
    [AppServices Login:dictparameter];
}
-(void)requestFriendsList
{
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result, NSError *error) {
        NSArray* friends = [result objectForKey:@"data"];
        NSLog(@"Found: %lu friends", (unsigned long)friends.count);
        for (NSDictionary<FBGraphUser>* friend in friends) {
            NSLog(@"I have a friend named %@ with id %@", friend.name, friend.objectID);
        }
    }];
    /*
     NSString *reqString=[NSString stringWithFormat:@"/me/friends?access_token=%@&fields=id,name,birthday,picture,location",[FBSession activeSession].accessTokenData];
     NSLog(@"request String %@",reqString);
     NSMutableArray *frndIDs=[[NSMutableArray alloc]init];
     NSMutableArray *friendNames=[[NSMutableArray alloc]init];
     //    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
     [FBRequestConnection startWithGraphPath:reqString parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
     if(error)
     NSLog(@"Display Error: %@", error.description);
     NSLog(@"result== %@",result);
     //Here the Resultes will be populated.
     NSArray *friends = [result objectForKey:@"data"];
     if (friends.count > 0)
     {
     for(NSDictionary *friend in friends)
     {
     FbFrnds *frnd  = [[FbFrnds alloc]init];
     frnd.frndId =     [friend objectForKey:@"id"];
     frnd.name   =     [friend objectForKey:@"name"];
     frnd.picURL =   [friend objectForKey:@"picture"];
     frnd.selected=NO;
     [frndIDs addObject:frnd.frndId];
     NSLog(@"frnd name %@" , frnd.name);
     NSLog(@"frndsCount%lu",(unsigned long)frndIDs.count);
     NSLog(@"frndList:%@",frnd.name);
     }
     }
     [MBProgressHUD hideHUDForView:MainWindow animated:YES];
     if (frndIDs.count>0)
     {
     for (NSString * friendID in frndIDs){
     NSString * query = [NSString stringWithFormat:@"SELECT uid1,uid2 FROM friend where uid1= %@ and uid2 in (SELECT uid2 FROM friend where uid1=me()) ", friendID];
     NSDictionary *queryParam = @{ @"q": query };
     // Make the API request that uses FQL
     [FBRequestConnection startWithGraphPath:@"/fql"
     parameters:queryParam
     HTTPMethod:@"GET"
     completionHandler:^(FBRequestConnection *connection,
     id result,
     NSError *error) {
     if (error) {
     
     NSLog(@"Error: %@", [error localizedDescription]);
     } else {
     
     NSLog(@"Result: %@", result);
     
     NSArray *mutualFrnds=[result objectForKey:@"data"];
     NSLog(@"mutual frnd %@",[mutualFrnds objectAtIndex:0]);
     NSDictionary *mtfrnd=[mutualFrnds objectAtIndex:0];
     NSString *mutaualFriendID=[mtfrnd objectForKey:@"uid2"];
     NSLog(@"mutal frnd id %@",mutaualFriendID);
     FbFrnds *FbFriend=[friendNames objectAtIndex:0];
     if ([FbFriend.frndId isEqualToString:mutaualFriendID])
     {
     NSLog(@"mutual friend pic%@",FbFriend.picURL);
     }
     }
     }];
     [MBProgressHUD hideHUDForView:MainWindow animated:YES];}
     }
     else
     {
     NSLog(@"No Friend Found");
     }
     }];*/
}
-(void)setup_Controls
{
    /*FBSession* session = [FBSession activeSession];
     [session closeAndClearTokenInformation];
     [session close];
     [FBSession setActiveSession:nil];*/
    isFBCall=NO;
    self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
    self.bannerView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    // Enable test ads on simulators.
    request.testDevices = @[ GAD_SIMULATOR_ID, @"MY_TEST_DEVICE_ID"];
    [self.bannerView loadRequest:request];
    FBLoginView *loginView =
    [[FBLoginView alloc] initWithReadPermissions:
     @[@"public_profile", @"email", @"user_friends",@"user_birthday",@"user_about_me"]];
    loginView.delegate=self;
    // Align the button in the center horizontally
    loginView.frame = CGRectOffset(loginView.frame, (self.view.center.x - (loginView.frame.size.width / 2)), self.view.center.y-(loginView.frame.size.height/2)+50);
    [self.view addSubview:loginView];
}
#pragma mark - Notify methods
-(void)userSignIn:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_Login object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    if ([status isEqualToString:@"success"])
    {
        [AppUtil setCurrentUserWithInfo:note.userInfo];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        NSString *msg=[note.userInfo objectForKey:@"msg"];
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}
-(void)userForgetPassword:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_ForgetPassword object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    NSString *msg=[note.userInfo objectForKey:@"msg"];
    if ([status isEqualToString:@"success"]) {
        [AppUtil displayAlertWithTitle:AlertTitle AndMessage:msg];
    }
    else
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}
@end
