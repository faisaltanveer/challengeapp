


#pragma mark HTTP Request header values

#define TIMEOUT_INTERVAL							30

//Facebook Constants
#define KToken                                  @"token"
#define KExpiryDate                             @"expiry_date"
#define KuserId									@"User_Id"
#define KFBEmailId							    @"userFBid"

#define kEmailKey                               @"kEmailKey"
#define kPasswordKey                            @"kPasswordKey"
#define kCurrentUser                            @"user"
/*******************************************************************************************************************/
#pragma mark - Strings

#define NSSTRING_HAS_DATA(_x) (((_x) != nil) && ( [(_x) length] > 0 ))

//NetManager
#define Error_General   @"Error!"
#define ERROR_INTERNET							@"Connection Error. Please check your internet connection and try again."
#define ERROR_TIMEOUT							@"Timeout error. Please check your internet connection and try again."
#define ERROR_INLOGIN                           @"Username OR Password is wrong."
//Near Me Tab


#pragma mark Alert title and messages
#define Alert_Cancel_Button                     @"Cancel"
#define Alert_Ok_Button                         @"Ok"
#define ERROR_GPS                               @"Error getting Current Location"
#define Login_Credential_Error                  @"Please enter your credentail for login."
#define NSSTRING_HAS_DATA(_x) (((_x) != nil) && ( [(_x) length] > 0 ))

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#pragma mark - Web Services Base Url and Methods

#define BaseUrl1                           @"http://iosapp.designers99.com/home/"
#define BaseUrl2                           @"http://www.appstudioz.com/demo/geodiction/web-services/"                       
#define BaseUrl                            @"http://iosapp.designers99.com/"

#define Method_ReadAll_Notifications       @"view_episodes.php"
#define Method_VarificationCode            @"sendsms"
#define Method_Signup                      @"usrSignUp"
#define Method_Send_CurrentLocation        @"updatecurrentlocation"

#define Method_Get_SuggestedFriends        @"getuserinfo"

#define Method_Send_Message                @"sendchatmessage"
#define Method_Add_Contact                 @"addusertolist"
#define Method_Receive_Message             @"getreceivedchatmessage"
#define Method_Send_NetworkStatus          @"setonlinestatus"
#define Method_Get_OnLineFriends           @"getalluseronlinestatus"
#define Method_Get_ContactsPrivacyList     @"getallrejecteduserlist"
#define Method_sendContactPrivacyStatus    @"addusertorejectedlist"
#define Method_ChatPush                    @"setusersetting"
#define Method_Login                       @"login"
#define Method_Update                      @"update_info"
#define Method_GetInfo                     @"pinfo"
#define Method_GetCategories               @"get_categories"
#define Method_GetChallenges               @"get_catgames"

#define Method_ForgetPassword              @"forget_password"
#define Method_LogOut                      @"logout"
/*******************************************************************************************************************/

#pragma mark- Notifications

//#define Notification_Facebook_success_Shared @"Notification_Facebook_success_Shared"

#define Notification_Did_Purchase_UnSuccess         @"Notification_Did_Purchase_UnSuccess"
#define Notification_Did_Purchase_Success           @"Notification_Did_Purchase_Success"
#define Notification_Facebook_User_Detail_Did_Load  @"Notification_Facebook_User_Detail_Did_Load"
#define Notification_Facebook_User_Cancled          @"Notification_Facebook_User_Cancled"
#define CompletedTwitter                            @"CompletedTwitter"
#define Notification_Facebook_success_Shared        @"Notification_Facebook_success_Shared"
#define Notification_Facebook_Did_Login             @"Notification_Facebook_Did_Login"
#define Notification_VarificationCode_Got           @"Notification_VarificationCode_Got"
#define Notification_Register_User                  @"Notification_Register_User"
#define Notification_GetUserInfo                    @"Notification_GetUserInfo"
#define Notification_GetChallengeCategories         @"Notification_GetChallengeCategories"
#define Notification_GetChallenges                  @"Notification_GetChallenges"
#define Notification_UpdateInfo                     @"Notification_UpdateInfo"
#define Notification_Send_Current_Location          @"Notification_Send_Current_Location"
#define Notification_Fetch_UserCurrent_Location     @"Notification_Fetch_UserCurrent_Location"
#define Notification_Fetch_SuggestedFriends         @"Notification_Fetch_SuggestedFriends"
#define Notification_Message_Sent                   @"Notification_Message_Sent"
#define Notification_ContactAdded                   @"Notification_ContactAdded"
#define Notification_Chat_Received                  @"Notification_Chat_Received"
#define Notification_NetworkStatus_Sent             @"Notification_NetworkStatus_Sent"
#define Notification_OnLineFriendsGot               @"Notification_OnLineFriendsGot"
#define Notification_ContactsPrivacyList_Got        @"Notification_ContactsPrivacyList_Got"
#define Notification_ContactPrivacyStatusSent       @"Notification_ContactPrivacyStatusSent"
#define Notification_ChatPush_SettingSaved          @"Notification_ChatPush_SettingSaved"
#define Notification_Login                          @"Notification_Login"
#define Notification_ForgetPassword                 @"Notification_ForgetPassword"
#define Notification_LogOut                         @"Notification_LogOut"