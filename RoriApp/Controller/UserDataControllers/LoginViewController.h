//
//  HomeViewController.h
//  GameApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GADBannerView;
@interface LoginViewController : UIViewController<FBLoginViewDelegate>
#pragma mark - Outlets
@property (strong, nonatomic) IBOutlet FBProfilePictureView *viewProfile;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

#pragma mark - Buttons action methods

- (IBAction)btn_login:(id)sender;
- (IBAction)btn_signUp:(id)sender;
- (IBAction)btn_ForgotPassword:(id)sender;

@end
