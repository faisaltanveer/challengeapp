//
//  SignUpViewController.m
//  RoriApp
//
//  Created by Hassan on 12/9/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "SignUpViewController.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize viewProfile,imgViewProfile,textFieldAge,textFieldEmail,textFieldContact,textFieldPassword,textFieldUserName;
#pragma mark - View laoding life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup_Controls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
} 
#pragma mark - Custom methods
-(void)setup_Controls
{
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField=40;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor=YES;
    //////////////////// add tap gesture to profile view //////////////////////
    UITapGestureRecognizer *tapOnProfile=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActionOnProfile)];
    [imgViewProfile addGestureRecognizer:tapOnProfile];
}
-(void)showActionSheet
{
    UIActionSheet *actionSheetProfile=[[UIActionSheet alloc]initWithTitle:@"Choose image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo library", nil];
    [actionSheetProfile showInView:self.view];
}
-(void)signUp
{
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    NSMutableDictionary *dictparameter=[[NSMutableDictionary alloc]init];
    [dictparameter setObject:textFieldUserName.text forKey:@"user_name"];
    [dictparameter setObject:textFieldEmail.text forKey:@"user_email"];
    [dictparameter setObject:textFieldContact.text forKey:@"user_phone"];
    //[dictparameter setObject:@"22" forKey:@"age"];
    [dictparameter setObject:textFieldPassword.text forKey:@"user_password"];
    [dictparameter setObject:@"contact" forKey:@"type"];
    NSData *dataProfileImg= UIImagePNGRepresentation(imgViewProfile.image);
    NSString *strProfileImg=[dataProfileImg base64EncodedString];
    [dictparameter setObject:strProfileImg forKey:@"user_pic_url"];
    //[dictparameter setObject:@"5464" forKey:@"user_fb_id"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userRegistered:) name:Notification_Register_User object:nil];
    [AppServices registerUser:dictparameter];
}
-(void)userRegistered:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_Register_User object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    if ([status isEqualToString:@"success"])
    {
        [AppUtil setCurrentUserWithInfo:note.userInfo];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        NSString *msg=[note.userInfo objectForKey:@"msg"];
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}
#pragma mark - Buttons action methods
-(void)tapActionOnProfile
{
    [self showActionSheet];
}
- (IBAction)btn_SignUp:(id)sender {
    if (isEmpty(self.textFieldUserName.text) || isEmpty(self.textFieldPassword.text) || isEmpty(self.textFieldContact.text) || isEmpty(self.textFieldEmail.text))
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:@"Please fill all fields"];
    }
    else if (!isEmail(self.textFieldEmail.text)) {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:@"Incorrect email."];
    }
    else
    {
        [self signUp];
    }
}
- (IBAction)btn_back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Acton sheet delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *actionSheetTitle=[actionSheet buttonTitleAtIndex:buttonIndex];
    UIImagePickerController *Picker = [[UIImagePickerController alloc] init];
    //Picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    Picker.delegate = self;
    //Picker.allowsEditing = YES;
    if ([actionSheetTitle isEqualToString:@"Camera"])
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            Picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:Picker animated:YES completion:NULL];
        }
        else
        {
            [AppUtil displayAlertWithTitle:AlertTitle AndMessage:@"Camera not available"];
        }
    }
    else if([actionSheetTitle isEqualToString:@"Photo library"])
    {
        Picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:Picker animated:YES completion:NULL];
    }
}
#pragma mark - Image pickerView delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chooseImage = info[UIImagePickerControllerOriginalImage];
    imgViewProfile.image=chooseImage;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
