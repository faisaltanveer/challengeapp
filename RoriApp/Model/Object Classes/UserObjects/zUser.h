//
//  zUser.h
//  SecondHandBooks
//
//  Created by Azeem Akram on 25/02/2013.
//  Copyright (c) 2013 @zeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface zUser : NSObject <NSCoding>{
    
    NSInteger userId;
    NSInteger groupId;
    NSInteger typeId;
    NSString *fullName;
    NSString *imagePath;
    NSString *email;
    NSString *password;
    NSString *name;
	
    BOOL     isNotificationActive;
    BOOL     isBlocked;
}

@property (nonatomic ,assign)  NSInteger groupId,userId,typeId;
@property (nonatomic,retain)   NSString *email,*password,*name,*fullName,*imagePath,*completeChallenges,*totalChallenges,*phoneNumber,*userRating;
@property (nonatomic , assign) BOOL  isNotificationActive,isBlocked;


-(id )initWithID:(NSInteger)usrID andTypeId:(NSInteger)typId  andEmail:(NSString *)zEmail  andPassword:(NSString *)pwd andUserName:(NSString *)nme andGroupId:(NSInteger)zgrpId andFullNameIs:(NSString *)fname andUserImageis:(NSString *)imagepath andIsNotificationActv:(BOOL)ntAcv andIsBlock:(BOOL)isBlock;

@end
