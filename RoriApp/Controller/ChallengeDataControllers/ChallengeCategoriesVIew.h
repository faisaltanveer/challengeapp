//
//  ChallengeCategoriesVIew.h
//  RoriApp
//
//  Created by Hassan on 22/12/2014.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChallengeCategoriesVIew : UIViewController
#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UIScrollView *sViewCategories;
#pragma mark - Buttons action methods
- (IBAction)btn_back:(id)sender;

@end
