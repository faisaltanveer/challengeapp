#import <UIKit/UIKit.h>

@interface SmoothedBIView : UIView
{
    UIImage *incrementalImage;
    CGPoint pts[5];
    uint ctr;
    NSSet *touches__;
    
    CGSize FinalImageSize;
}

@property(nonatomic,retain) UIBezierPath    *Path;
@property(nonatomic,retain) UIColor         *Color;
@property(nonatomic,retain) UIImage *FinalImage;

@end