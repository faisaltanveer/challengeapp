//
//  Services.m
//  DriverModule
//
//  Created by appstudioz on 05/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Services.h"
#import "AppUtility.h"
#import "StringTransformer.h"
//#import "AFNetworking.h"
//#import "AFHTTPClient.h"
@implementation Services

#pragma mark - Initialize methods
+ (Services*) sharedInstance
{
	static Services* singleton;
    
	if (!singleton)
	{
		singleton = [[Services alloc] init];
	}
	return singleton;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        // Initialization code here.
    }
    return self;
}
#pragma mark - User registeration mehtods
-(void)registerUser:(NSMutableDictionary *)dictionary
{
    NSLog(@"parameters == %@",dictionary);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl1,Method_Signup];
    NSLog(@"url==%@",strUrl);
    [manager POST:strUrl parameters:dictionary
     success:^(AFHTTPRequestOperation *operation, NSDictionary *jsondict)
     {
         NSLog(@"JSON: %@", jsondict);
         if (jsondict.count>0)
         {
             NSString *msg=[jsondict objectForKey:@"msg"];
             if ([msg isEqualToString:@"Email Already Exists !"]) {
                 NSString *email=[dictionary objectForKey:@"user_email"];
                 NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
                 [parameters setObject:email forKey:@"login_id"];
                 [parameters setObject:@"password" forKey:@"user_password"];
                 [self Login:parameters];
                 return;
             }
             if([jsondict isKindOfClass:[NSDictionary class]])
                 [[NSNotificationCenter defaultCenter] postNotificationName:Notification_Register_User  object:nil userInfo:jsondict];
         }
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [MBProgressHUD hideHUDForView:MainWindow animated:YES];
         [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
         return;
     }];
}
-(void)Login:(NSMutableDictionary *)dictionary
{
    NSLog(@"dictionary=%@",dictionary);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl,Method_Login];
    NSLog(@"url==%@",strUrl);
    [manager POST:strUrl parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
        if([responseObject isKindOfClass:[NSDictionary class]])
            [[NSNotificationCenter defaultCenter] postNotificationName:Notification_Login  object:nil userInfo:responseObject];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:MainWindow animated:YES];
        [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
    }];
}
-(void)forgetPassword:(NSMutableDictionary *)dictionary
{
    NSLog(@"dictionary=%@",dictionary);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl,Method_Login];
    NSLog(@"url==%@",strUrl);
    [manager POST:strUrl parameters:dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response: %@", responseObject);
         if([responseObject isKindOfClass:[NSDictionary class]])
             [[NSNotificationCenter defaultCenter] postNotificationName:Notification_ForgetPassword  object:nil userInfo:responseObject];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [MBProgressHUD hideHUDForView:MainWindow animated:YES];
              [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
          }];
}
#pragma mark - Challenge related methods
-(void)getChallengesForCategory:(NSMutableDictionary*)dictionary
{
    NSLog(@"dictionary=%@",dictionary);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl1,Method_GetChallenges];
    NSLog(@"url ==%@",strUrl);
    [manager POST:strUrl parameters:dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         if([responseObject isKindOfClass:[NSDictionary class]])
             [[NSNotificationCenter defaultCenter] postNotificationName:Notification_GetChallenges  object:nil userInfo:responseObject];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [MBProgressHUD hideHUDForView:MainWindow animated:YES];
              [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
          }];
}
-(void)getChallengeCategories
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl1,Method_GetCategories];
    NSLog(@"url==%@",strUrl);
    [manager POST:strUrl parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSString *decodedString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         NSDictionary *json=[decodedString objectFromJSONString];
         NSLog(@"JSON: %@", json);
         if([json isKindOfClass:[NSDictionary class]])
             [[NSNotificationCenter defaultCenter] postNotificationName:Notification_GetChallengeCategories  object:nil userInfo:json];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [MBProgressHUD hideHUDForView:MainWindow animated:YES];
              [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
          }];
}
#pragma mark - User's info methods
-(void)getUserInfo:(NSMutableDictionary *)dictionary
{
    NSLog(@"dictionary=%@",dictionary);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl1,Method_GetInfo];
    NSLog(@"url user info==%@",strUrl);
    [manager POST:strUrl parameters:dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         if([responseObject isKindOfClass:[NSDictionary class]])
             [[NSNotificationCenter defaultCenter] postNotificationName:Notification_GetUserInfo  object:nil userInfo:responseObject];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [MBProgressHUD hideHUDForView:MainWindow animated:YES];
              [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
          }];
}
-(void)updateInfo:(NSMutableDictionary *)dictionary
{
    NSLog(@"dictionary=%@",dictionary);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",BaseUrl1,Method_Update];
    NSLog(@"url==%@",strUrl);
    [manager POST:strUrl parameters:dictionary
    success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         if([responseObject isKindOfClass:[NSDictionary class]])
             [[NSNotificationCenter defaultCenter] postNotificationName:Notification_UpdateInfo  object:nil userInfo:responseObject];
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [MBProgressHUD hideHUDForView:MainWindow animated:YES];
              [AppUtil displayAlertWithTitle:Error_General AndMessage:error.localizedDescription];
          }];
}

@end