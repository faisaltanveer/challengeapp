//
//  DBManager.h
//  RoriApp
//
//  Created by Hassan on 12/11/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <Foundation/Foundation.h>

#define AppDBManager [DBManager sharedWHNUtility]
@interface DBManager : NSObject
+(DBManager *)sharedWHNUtility;
- (NSManagedObjectContext *)managedObjectContext;
@end
