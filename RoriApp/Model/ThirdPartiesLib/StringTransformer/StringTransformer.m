//
//  StringTransformer.m
//  Pepsi
//
//  Created by Admin on 08.04.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StringTransformer.h"


@implementation StringTransformer

+ (NSString *)transformDecodedString:(NSString *)decodedString {
//NSLog(@"!!!!!!decoded string not transformed = %@", decodedString);
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\r\\n" withString:@""];//       

//	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\\"\\\"" withString:@"\"\""]; //  \"\" 

	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\"\\\"" withString:@"\""];    // "\" "

	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\\"\"" withString:@"\""];

	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\\\\"" withString:@"\\\""];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\"{" withString:@"{"];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"}\"" withString:@"}"];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\"[" withString:@"["];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"]\"" withString:@"]"];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
	decodedString = [decodedString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    decodedString = [decodedString stringByReplacingOccurrencesOfString:@"([" withString:@"["];
    decodedString = [decodedString stringByReplacingOccurrencesOfString:@"])" withString:@"]"];
    

	
	NSInteger begin;
	NSString *findValue;
    
	if ([decodedString rangeOfString:@">{"].location != NSNotFound) 
    {
		findValue = @">{";
	} 
    else 
    {
		findValue = @">[";
	}
	
	begin = [decodedString rangeOfString:findValue].location + 1;

	NSInteger end = [decodedString length] - 9 - begin;
	if (begin < [decodedString length] && (end + begin < [decodedString length])) {
		decodedString = [decodedString substringWithRange:NSMakeRange(begin, end)];
	}
	//NSLog(@"!!!!!!decoded string transformed = %@", decodedString);
	return decodedString;
}

+ (NSString *)contentVersionFromDecodedString:(NSString *)decodedString {
	//NSLog(@"decoded string = %@", decodedString);
	NSInteger begin;
	begin = [decodedString rangeOfString:@"jakten.no/\">"].location + 12;
	//begin = [decodedString rangeOfString:@"mpuri.org/\">"].location + 12;
	
	NSInteger end = [decodedString length] - 9 - begin;
	if (begin < [decodedString length] && (end + begin < [decodedString length])) {
		decodedString = [decodedString substringWithRange:NSMakeRange(begin, end)];
	}
	return decodedString;
}

+(NSString*)htmlStringWithFontSize:(NSInteger)fontSize fontFamily:(NSString*)fontFamily content:(NSString*)content
{
   return [NSString stringWithFormat:@"<html> \n"
     "<head> \n"
     "<style type=\"text/css\"> \n"
     "body {font-family: \"%@\"; font-size: %@; vertical-align:middle;text-align:center;}\n"
     "</style> \n"
     "</head> \n"
     "<body>%@</body> \n"
     "</html>", fontFamily, [NSNumber numberWithInteger:fontSize], content];
}
+(NSString*)gethtmlStringWithFontSize:(NSInteger)fontSize fontFamily:(NSString*)fontFamily content:(NSString*)content
{
    return [NSString stringWithFormat:@"<html> \n"
            "<head> \n"
            "<style type=\"text/css\"> \n"
            "body {font-family: \"%@\"; font-size: %@; vertical-align:middle;}\n"
            "</style> \n"
            "</head> \n"
            "<body>%@</body> \n"
            "</html>", fontFamily, [NSNumber numberWithInteger:fontSize], content];
}
@end
