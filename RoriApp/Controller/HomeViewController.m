//
//  HomeViewController.m
//  RoriApp
//
//  Created by Hassan on 12/9/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "HomeViewController.h"
#import "LoginViewController.h"
#import "ProfileViewClass.h"
#import "SettingViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setupControls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action methods
- (IBAction)btn_viewProfile:(id)sender {
    ProfileViewClass *pVC=[self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
    [self.navigationController pushViewController:pVC animated:YES];
}
- (IBAction)btn_settings:(id)sender {
    SettingViewController *sVC=[self.storyboard instantiateViewControllerWithIdentifier:@"setting"];
    [self.navigationController pushViewController:sVC animated:YES];
}
#pragma mark -Custom methods
-(void)setupControls
{
    if (AppUtil.cUser.name.length<1) {
        LoginViewController *lVC=[self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:lVC animated:NO completion:nil];
    }
}
@end