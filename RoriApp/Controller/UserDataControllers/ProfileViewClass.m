//
//  ProfileViewClass.m
//  RoriApp
//
//  Created by Hassan on 12/18/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "ProfileViewClass.h"

@interface ProfileViewClass ()

@end

@implementation ProfileViewClass
@synthesize imgViewProfile,lblChallengeCompleted,lblChallengeGiven,lblUserName;
@synthesize lblUserRating;
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
   [self get_userInfo];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action methods
- (IBAction)btn_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Custom methods
-(void)setUserInfo
{
    NSURL *imgURL=[NSURL URLWithString:AppUtil.cUser.imagePath];
    [imgViewProfile setImageURL:imgURL];
    lblUserName.text=AppUtil.cUser.name;
    lblChallengeCompleted.text=AppUtil.cUser.completeChallenges;
    lblChallengeGiven.text=AppUtil.cUser.totalChallenges;
    lblUserRating.text=AppUtil.cUser.userRating;
}
-(void)get_userInfo
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[NSNumber numberWithInteger:AppUtil.cUser.userId] forKey:@"user_id"];
    [MBProgressHUD showHUDAddedTo:MainWindow animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userGotInfo:) name:Notification_GetUserInfo object:nil];
    [AppServices getUserInfo:dict];
}
-(void)userGotInfo:(NSNotification *)note
{
    NSLog(@"json response==%@",note.userInfo);
    if(note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Notification_GetUserInfo object:nil];
    }
    NSString *status=[note.userInfo objectForKey:@"status"];
    NSString *msg=[note.userInfo objectForKey:@"msg"];
    if ([status isEqualToString:@"success"])
    {
        [AppUtil setCurrentUserWithInfo:note.userInfo];
        [self setUserInfo];
    }
    else
    {
        [AppUtil displayAlertWithTitle:Error_General AndMessage:msg];
    }
    [MBProgressHUD hideHUDForView:MainWindow animated:YES];
}
@end
