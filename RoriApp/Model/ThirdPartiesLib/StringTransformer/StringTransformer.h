//
//  StringTransformer.h
//  Pepsi
//
//  Created by Admin on 08.04.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface StringTransformer : NSObject {

	
}
+ (NSString *)transformDecodedString:(NSString *)decodedString;
+ (NSString *)contentVersionFromDecodedString:(NSString *)decodedString;
+(NSString*)htmlStringWithFontSize:(NSInteger)fontSize fontFamily:(NSString*)fontFamily content:(NSString*)content;
+(NSString*)gethtmlStringWithFontSize:(NSInteger)fontSize fontFamily:(NSString*)fontFamily content:(NSString*)content;
@end
