//
//  ChallengerView.m
//  RoriApp
//
//  Created by Hassan on 20/12/2014.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "ChallengerView.h"

@interface ChallengerView ()

@end

@implementation ChallengerView
#pragma mark - Views loadikng life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action methods
- (IBAction)btn_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
