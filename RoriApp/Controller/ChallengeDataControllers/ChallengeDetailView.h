//
//  ChallengeDetailView.h
//  GameApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChallengeDetailView : UIViewController
#pragma mark - properties
@property(nonatomic,retain)UIImage *imgChallenge;
#pragma mark - View outlets
@property (weak, nonatomic) IBOutlet UIImageView *imgViewChallenge;
@end
