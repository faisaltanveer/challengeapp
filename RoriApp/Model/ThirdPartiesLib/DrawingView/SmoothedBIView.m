#import "SmoothedBIView.h"
//#import "DataManager.h"

@implementation SmoothedBIView

@synthesize Path = _Path;
@synthesize Color = _Color;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setMultipleTouchEnabled:NO];
        self.Path=[[UIBezierPath alloc]init];
        self.Path.lineCapStyle=kCGLineJoinRound;
        self.Path.miterLimit = 0;
        self.Path.lineWidth = 6.0;
        self.FinalImage = nil;
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setMultipleTouchEnabled:NO];
        self.Path=[[UIBezierPath alloc]init];
        self.Path.lineCapStyle=kCGLineJoinRound;
        self.Path.miterLimit = 0;
        self.Path.lineWidth = 6.0;
        self.Color =  [UIColor whiteColor];
        self.FinalImage = nil;
    }
    return self;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *Touch = [touches anyObject];
    CGPoint StartPoint = [Touch locationInView:self];
    [self.Path moveToPoint:StartPoint];
    [self setNeedsDisplay];
    ctr = 0;
    pts[0] = StartPoint;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *Touch = [touches anyObject];
    CGPoint Point = [Touch locationInView:self];
    ctr++;
    pts[ctr] = Point;
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0);
        [self.Path moveToPoint:pts[0]];
        [self.Path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];
        [self drawBitmap];
        [self setNeedsDisplay];
        [self.Path removeAllPoints];
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
        [self.Path moveToPoint:pts[0]];
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *Touch = [touches anyObject];
    CGPoint Point = [Touch locationInView:self];
    [self.Path addLineToPoint:Point];
    [self drawBitmap];
    [self setNeedsDisplay];
    [self.Path removeAllPoints];
    ctr = 0;
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}
- (void)drawRect:(CGRect)rect
{
    [incrementalImage drawInRect:rect];
    [self.Color setStroke];
    [incrementalImage drawInRect:rect];
    [self.Path strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
}
- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    if (!incrementalImage)
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [[UIColor clearColor] setFill];
        [rectpath fill];
    }
    [incrementalImage drawAtPoint:CGPointZero];
    [self.Color setStroke];
    [self.Path strokeWithBlendMode:kCGBlendModeCopy alpha:1.0];
    incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.FinalImage = incrementalImage;
}
@end