//
//  ChallengeCollectionView.h
//  GameApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChallengeCollectionView : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UICollectionView *cViewChallenges;

@end
