//
//  ChallengeDetailView.m
//  GameApp
//
//  Created by Hassan on 12/5/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import "ChallengeDetailView.h"
#import "ChallengeCollectionView.h"
@interface ChallengeDetailView ()

@end

@implementation ChallengeDetailView
@synthesize imgViewChallenge,imgChallenge;
#pragma mark - Views loading life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup_Controls];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons action methods
-(void)btn_selectUsers : (id)sender
{
    NSLog(@"select users");
    ChallengeCollectionView *cVC=[self.storyboard instantiateViewControllerWithIdentifier:@"challengeCollection"];
    cVC.navigationItem.title=@"Select Users";
    [self.navigationController pushViewController:cVC animated:YES];
}
#pragma mark - Custom methods
-(void)setup_Controls
{
    self.navigationItem.title=@"Challenge Detail";
    imgViewChallenge.image=imgChallenge;
    UIBarButtonItem *selectUsers=[[UIBarButtonItem alloc]initWithTitle:@"Select users" style:UIBarButtonItemStylePlain target:self action:@selector(btn_selectUsers:)];
    self.navigationItem.rightBarButtonItem=selectUsers;
}
@end
