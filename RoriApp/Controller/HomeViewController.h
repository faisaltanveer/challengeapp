//
//  HomeViewController.h
//  RoriApp
//
//  Created by Hassan on 12/9/14.
//  Copyright (c) 2014 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
#pragma mark - Buttons action methods
- (IBAction)btn_viewProfile:(id)sender;
- (IBAction)btn_settings:(id)sender;

@end
